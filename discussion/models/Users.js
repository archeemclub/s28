const mongoose = require('mongoose');

const userBlueprint = newm mongoose.Schema({

});
new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required'] 
	} ,
	lastName: {
		type: String,
		required: [true, 'Last Name is Required']
	} ,
	email:  {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	} ,
	isAdmin: {
		type: Boolean,
		default: false
	} ,
	tasks: [
		{
			taskId: {
				type: String,
				required: [true, 'Task Id is required']
			},
			assignedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userBlueprint);