// [SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./route/task')

// [SECTION] Environment Variables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING;

//  [SECTION] Server Setup
	const server = express();
	server.use(express.json());
	const port = 4000;

// [SECTION] Server Routes
	server.use('/tasks', taskRoutes);

// [SECTION] Database Connection
	mongoose.connect(secret);
	let db = mongoose.connection;
	db.once('open', () => console.log('Now Connected Pakers!'));

// [SECTION] Server Response
	server.listen(port, () => console.log(`Tumatakbo na ang iyong server! ${port}`));