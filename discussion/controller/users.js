// [SECTION] Dependencies and Modules
const user = require('../models/User');

//[SECTION] User Functionalities
module.exports.registerUser = (reqBody) => {
	let fName = reqBody.firstName;	
	let lName = reqBody.lastName;
	let email = reqBody.email;
	let passW = reqBody.password;	

	let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: passW
	});

	return newUser.save().then((user, error) => {
		if (user) {
			return 'Bagong account ang nagawa';
		} else {
			return	'Di ka nagawa ng account';
		}
	});
};