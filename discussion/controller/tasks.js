// [SECTION] Dependencies and Modules
const Task = require('../models/Tasks');

// [SECTION] Functionalities
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name
		let newTask = new Task({
			name: taskName

		});
		return newTask.save().then((task, error) => {
			if (error) {
				return 'Palpak ang pag trabaho';
			} else {
				return 'Tama ang ginawang trabaho';
			}

		})
	}

	module.exports.getAllTasks =() =>{
		return Task.find({}).then(searchResult => {
			return searchResult;
		})
	}