const exp = require('express')
const controller = require('../controllers/users')
const route = exp.Router()

route.post('/register',(req,res)=>{
	let data =req.body;
	controller.registerUser(data).then(result=>
		res.send(result));


})

// [SECTION] Expose Routing System
	module.exports =route;