//[SECTION] Dependencies and Modules
	const express = require('express'); 
	const controller = require('../controllers/tasks');

//[SECTION] Routing Component
	const route = express.Router(); 

//[SECTION] Tasks Routes
    //Create Task
	route.post('/', (req, res) => {
		//execute the createTask() from the controller.
		let taskInfo = req.body;
		controller.createTask(taskInfo).then(result => 
			res.send(result)
		)
	}); 

	//Retrieve all Tasks 
	route.get('/', (req, res) => {
		controller.getAllTasks().then(result => {
			res.send(result);
		})
	}); 

//[SECTION] Expose Route System
	module.exports = route; 
